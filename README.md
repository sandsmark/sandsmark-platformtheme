# Framework Integration

Integration of Qt application with Sandsmark's hacky setup.

## Introduction

Framework Integration is a set of plugins responsible for better integration of
Qt applications when running on Sandsmark's hacky setup.

Applications do not need to link to this directly.

## Components

### KDEPlatformTheme

The plugin SandsmarkPlatformTheme provides KDE integration for
QPlatformThemePlugin.

